window.onload = () => {
    const btnDonate = document.querySelector('.page-header__buttons .btn-donate');
    const infoCard = document.querySelector('.homepage .info-card');

    if (btnDonate) {
        btnDonate.addEventListener('click', (e) => {
            e.preventDefault();

            infoCard.scrollIntoView({behavior: 'smooth'});
        });
    }
};

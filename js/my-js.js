/**
 * Created by паша on 16.06.2018.
 */

$(".dropdown-menu li a").click(function(ev){
  ev.preventDefault();
  var selText = $(this).text();
  $(this).parents('.btn-group').find('.dropdown-toggle').html(selText);
});

$('.js-form-switcher').on('change',function(){
var parents_block = $(this).parents(".js-form-block");
if ($('.js-form-switcher').is(":checked")) {
  parents_block.find('.js-form-item').hide();
  parents_block.find('.js-form-show').toggleClass('active');
  $('.js-signup-form').show().addClass('active');
} else {
  parents_block.find('.js-form-item').hide();
  parents_block.find('.js-form-show').toggleClass('active');
  $('.js-login-form').show().addClass('active');
}
});

$('.js-form-show').on('click',function(ev){
ev.preventDefault();
var curTab = $(this).attr('data-show');
var parents_block = $(this).parents(".js-form-block");
parents_block.find('.js-form-show').removeClass('active');
$(this).addClass('active');
parents_block.find('.js-form-item').hide().removeClass('active');
parents_block.find('[data-form="' +curTab + '"]').show().addClass('active');
    if (parents_block.find('.js-signup-form').hasClass("active")) {
      $('.js-form-switcher').prop('checked',true)
    } else {
      $('.js-form-switcher').prop('checked',false)
    }
});
$(".js-reset-show").on('click',function(ev){
ev.preventDefault();
var parents_block = $(this).parents(".js-form-block");
parents_block.find('.js-form-item').hide();
$(".js-reset-form").show();
});
$(".js-login-show").on('click',function(ev){
ev.preventDefault();
var parents_block = $(this).parents(".js-form-block");
parents_block.find('.js-form-item').hide();
$(".js-login-form").show();
});
$(".js-show-filter").on('click',function(ev){
ev.preventDefault();
$(".js-filter-block").show();
});
$(".js-hide-filter").on('click',function(ev){
ev.preventDefault();
$(".js-filter-block").hide();
})
$(".js-show-smiles").on('click', function(){
$(".js-smiles-block").toggle();
});

$(".js-make-fullscreen").click(function(){
  console.log('envoken')
  $(".js-video").addClass("full-screen");
})
document.onkeydown = function(evt) {
if (evt.keyCode == 27) {
    console.log("123")
  $(".js-video").removeClass("full-screen");
}
};
var old_classes = $(".js-video-column").attr('class');
$(".js-toggle-chat").on('click',function(){
  console.log(old_classes)
  if ($(this).hasClass("pressed")) {
    $(".js-chat-column").show();
    $(".js-video-column").attr('class', old_classes)
    $(this).removeClass("pressed");
  } else {
      $(".js-chat-column").hide();
      $(".js-video-column").attr('class', 'js-video-column col-12 col-md-12 col-lg-12 col-xl-12 col-sm-12');
      $(this).addClass("pressed");
  }
});
